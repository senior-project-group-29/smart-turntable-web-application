import './Library.css'
import React from "react";
import AlbumTable from "../../components/table/AlbumTable"
import TrackTable from "../../components/table/TrackTable"
import AlbumGrid from "../../components/AlbumGrid/AlbumGrid"

function Library(){
    return(
        <div>
            <h1 className='albums'>Library</h1><br/>
            <AlbumTable />
        </div>
    );
}
    
export default Library;