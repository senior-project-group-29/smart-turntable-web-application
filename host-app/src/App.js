import React, {useState, useEffect} from 'react';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import Home from './pages/Home/Home';
import Library from './pages/Library/Library';
import Options from './pages/Options/Options';
import Aboutus from './pages/Aboutus/Aboutus';
import TracksOnAlbum from './pages/TracksOnAlbum/TracksOnAlbum';
import Recordings from './pages/Recordings/Recordings';
import EditTracks from './pages/EditTracks/EditTracks';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";

// Home page on the web application
function App() {
  //returns components to display on the page
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/home" element={<Home />}></Route>
          <Route path="/library" element={<Library />}></Route>
          <Route path="/recordings" element={<Recordings />}></Route>
          <Route path="/aboutus" element={<Aboutus />}></Route>
          <Route path="/album" element={<TracksOnAlbum />}></Route>
          <Route path="/edittracks" element={<EditTracks />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
