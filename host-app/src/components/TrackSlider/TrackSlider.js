import React, { Component, useState} from "react";
import Grid from '@mui/material/Grid'

function saveTime(){
    
}

class TrackSlider extends Component{

    render(){
        return(
            <div>
                <br/>
                <br/>
                <Grid container spacing={2} alignItems="center">
                    <Grid item xs={6}>
                        <div>{this.props.TrackName}</div>
                    </Grid>
                    <Grid item xs={2}>
                        <div>
                            <input type="text" id="start" name="start" value={this.props.TrackStartTime} size="3"></input>
                        </div>
                    </Grid>
                    <Grid item xs={2}>
                        <div>
                            <input type="text" id="stop" name="stop" value={this.props.TrackStopTime} size="3"></input>
                        </div>
                    </Grid>
                    <Grid item xs={2}>
                        <div>
                            <button onClick={saveTime()}>Save</button>
                        </div>
                    </Grid>
                </Grid>
            </div>
        )
    }
}


export default TrackSlider