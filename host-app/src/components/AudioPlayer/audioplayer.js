import React from 'react'
import Actions from '../Actions'

let AudioPlayer = () => {
    return <div className="AudioPlayer">
        <div className="inside-content">
            <Actions />
        </div>
    </div>
}

export default AudioPlayer