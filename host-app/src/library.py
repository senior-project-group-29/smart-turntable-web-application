import json
import sqlite3 as sql

def getData():
    con = sql.connect("src/db/stt.db")
    cur = con.cursor()
    dataSet = {}

    for row in cur.execute("SELECT * FROM album"):
        # album_id | title | catalog_number | a_date | a_length | track_list | artist_id | label_id | studio_id
        key = row[0]
        values = [row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8]]
        dataSet[key] = values
    json_dump = json.dumps(dataSet)
    print(json_dump)


if __name__ == '__main__':
    getData()