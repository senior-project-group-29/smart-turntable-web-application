import React, { Component, useState, useEffect} from "react";
import Grid from '@mui/material/Grid'
import "./EditTracks.css"
import TrackSlider from '../../components/TrackSlider/TrackSlider.js'



function EditTracks() {
    const [rec, setRecData] = useState([{}])

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id')

    useEffect(() => {
        fetch("./api/recording?id=" + id).then(
            res => res.json()
        ).then(
            rec => {
              setRecData(rec)
              console.log(rec)
            }
        )
    }, [])

    const [tracks, setTrackData] = useState([{}])

    useEffect(() => {
        fetch("./api/all_track_files?id=" + 1).then(
            res => res.json()
        ).then(
            tracks => {
            setTrackData(tracks)
              console.log(tracks)
            }
        )
    }, [])

    return(
        <div>
            {
                rec.map((item) => (
                <Grid container spacing={2} direction="column" alignItems="center">
                    <Grid item xs={6}>
                        <div>Recording field&emsp;
                            <i>{item.f_name}</i>
                        </div>
                    </Grid>
                    <Grid item xs={6}>
                        <div>Linked to Album&emsp;
                            <i>{item.title}</i>
                        </div>
                    </Grid>
                </Grid>
                ))
            }
            {tracks.map((track) =>
            <div>
                <TrackSlider TrackStopTime={track.stop_time} TrackStartTime={track.start_time} TrackName={track.f_name}/>
            </div>
            )
            }
        </div>
    )
}

export default EditTracks;