import React, { Component, useState, useEffect } from "react";
import MaterialTable from "material-table";
import tableIcons from "./MaterialTableIcons";

const columns = [
  { title: "Name", field: "title" },
  { title: "Length", field: "track_length" },
];

function TrackTable({ albumID }) {
  const [trackData, setTrackData] = useState([{}]);
  console.log(albumID);

  useEffect(() => {
    fetch("./api/track?id=" + albumID)
      .then((res) => res.json())
      .then((trackData) => {
        setTrackData(trackData);
        console.log(trackData);
      });
  }, []);

  return (
    <div>
      <MaterialTable
        title="Tracks"
        icons={tableIcons}
        columns={columns}
        data={trackData}
        options={{
          headerStyle: {
            backgroundColor: "#081129",
            color: "#FFF",
            fontSize: "17px",
            textAlign: "center",
            fontWeight: "bold",
          },
        }}
      />
    </div>
  );
}

export default TrackTable;
