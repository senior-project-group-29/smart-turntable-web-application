//Items to be displayed by the navbar
//PageName = the displayed title of the clickable button
//url = the page the button will take you to when clicked on
//cName = the class name of the navbar item
export const NavbarItems = [
    {
        PageName: 'Home',
        url: 'home',
        cName: 'nav-links'
    },
    {
        PageName: 'Library',
        url: 'library',
        cName: 'nav-links'
    },
    {
        PageName: 'Recordings',
        url: 'recordings',
        cName: 'nav-links'
    },
    {
        PageName: 'About Us',
        url: 'aboutus',
        cName: 'nav-links'
    }
]