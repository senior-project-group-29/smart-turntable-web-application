import React from "react";
import TrackTable from "../../components/table/TrackTable"

function TracksOnAlbum(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id')
    console.log(id)

    return(
        <div>
            <h1 className='tracks'>Tracks On Album</h1><br/>
            <TrackTable albumID={id} />
        </div>
    );
}
    
export default TracksOnAlbum;