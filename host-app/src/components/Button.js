import React from "react";
import './Button.css';

//Button styles
const STYLES = [
    'btn--primary',
    'btn--outline'
]

//Buttons sizes
const SIZES = [
    'btn--medium',
    'btn--large'
]

//Construct button
export const Button = ({
    children,
    type,
    onClick,
    buttonStyle,
    buttonSize
}) => {
    //If the proviaded button has a style and size set it to that, otherwise set it to the first STYLE and SIZES 
    const checkButtonStyle = STYLES.includes(buttonStyle) ? buttonStyle : STYLES[0]

    const checkButtonSize = SIZES.includes(buttonSize) ? buttonSize : SIZES[0]

    //return the html that constructs the button given the variables from above
    return (
        <button className={'btn ${checkButtonStyle} ${checkButtonSize}'} onClick={onClick} type={type}>
            {children}
        </button>
    )
}